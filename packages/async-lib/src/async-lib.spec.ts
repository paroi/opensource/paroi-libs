import { enqueueAsync, enqueueOrMergeAsync, promiseToHandle, rejectOnTimeout, wait } from './async-lib';

describe('Helpers - async-lib', () => {
  describe('wait', () => {
    it('should wait 5 ms', async () => {
      const timer = createTimer();
      await expect(wait(5)).resolves.toBeUndefined();
      expect(timer.elapsedTimeIsAbout(5)).toBe(true);
    });
  });

  describe('promiseToHandle', () => {
    it('should return a promise that can be resolved', async () => {
      const value = 'abc';
      const { promise, resolve } = promiseToHandle<string>();
      resolve(value);
      expect(promise).resolves.toBe(value);
    });

    it('should return a promise that can be rejected', async () => {
      const { promise, reject } = promiseToHandle<string>();
      reject(new Error());
      expect(promise).rejects.toThrow();
    });
  });

  describe('rejectOnTimeout', () => {
    it('should return a promise that returns fast', async () => {
      const promise = rejectOnTimeout(
        wait(5).then(() => 'abc'),
        { maxDurationMs: 50 }
      );
      expect(promise).resolves.toBe('abc');
    });

    it('should reject a promise too slow', async () => {
      const promise = rejectOnTimeout(
        wait(50).then(() => 'abc'),
        { maxDurationMs: 5 }
      );
      expect(promise).rejects.toThrowError();
    });
  });

  describe('enqueueAsync(singleTask, options)', () => {
    const taskDurationMs = 20;
    let task: jest.Mock<Promise<void>>;

    beforeEach(() => {
      task = jest.fn(() => new Promise<void>((resolve) => setTimeout(resolve, taskDurationMs)));
    });

    it('pass parameters to the task', async () => {
      const _p1 = { v: 'ab' };
      const _p2 = { v: 'cd' };
      const _result = { v: 'ef' };
      const enqueuedTask = jest.fn(
        enqueueAsync((p1: any, p2: any) => {
          expect(p1).toBe(_p1);
          expect(p2).toBe(_p2);
          return Promise.resolve(_result);
        })
      );
      await expect(enqueuedTask(_p1, _p2)).resolves.toBe(_result);
    });

    it('should execute immediatly the first task', async () => {
      const enqueuedTask = enqueueAsync(task);
      const timer = createTimer();
      const promise = enqueuedTask();
      expect(task.mock.calls.length).toBe(1);

      await expect(promise).resolves.toBeUndefined();
      expect(timer.elapsedTimeIsAbout(taskDurationMs)).toBe(true);
    });

    it('should enqueue 3 tasks without delay', async () => {
      const enqueuedTask = enqueueAsync(task);
      expect(task.mock.calls.length).toBe(0);

      const timer = createTimer();
      const t1 = enqueuedTask();
      const t2 = enqueuedTask();
      const t3 = enqueuedTask();
      expect(task.mock.calls.length).toBe(1);

      await expect(t1).resolves.toBeUndefined();
      expect(task.mock.calls.length).toBe(2); // the next task is already started here
      expect(timer.elapsedTimeIsAbout(taskDurationMs)).toBe(true);

      await expect(t2).resolves.toBeUndefined();
      expect(task.mock.calls.length).toBe(3); // the next task is already started here
      expect(timer.elapsedTimeIsAbout(taskDurationMs * 2)).toBe(true);

      await expect(t3).resolves.toBeUndefined();
      expect(task.mock.calls.length).toBe(3);
      expect(timer.elapsedTimeIsAbout(taskDurationMs * 3)).toBe(true);
    });

    it('should enqueue 3 tasks with a delay between each', async () => {
      const delayMs = 50;
      const enqueuedTask = enqueueAsync(task, { delayMs });
      expect(task.mock.calls.length).toBe(0);

      const timer = createTimer();
      const t1 = enqueuedTask();
      const t2 = enqueuedTask();
      const t3 = enqueuedTask();
      expect(task.mock.calls.length).toBe(1);

      await expect(t1).resolves.toBeUndefined();
      expect(task.mock.calls.length).toBe(1);
      expect(timer.elapsedTimeIsAbout(taskDurationMs)).toBe(true);

      await expect(t2).resolves.toBeUndefined();
      expect(task.mock.calls.length).toBe(2);
      expect(timer.elapsedTimeIsAbout(Math.max(taskDurationMs, delayMs) + taskDurationMs)).toBe(true);

      await expect(t3).resolves.toBeUndefined();
      expect(task.mock.calls.length).toBe(3);
      expect(timer.elapsedTimeIsAbout(Math.max(taskDurationMs, delayMs) * 2 + taskDurationMs)).toBe(true);
    });
  });

  describe('enqueueAsync(options) with 2 tasks', () => {
    const task1DurationMs = 20;
    const task2DurationMs = 40;
    let task1: jest.Mock<Promise<void>>;
    let task2: jest.Mock<Promise<void>>;

    beforeEach(() => {
      task1 = jest.fn(() => new Promise<void>((resolve) => setTimeout(resolve, task1DurationMs)));
      task2 = jest.fn(() => new Promise<void>((resolve) => setTimeout(resolve, task2DurationMs)));
    });

    it('pass parameters to the task', async () => {
      const _p1 = { v: 'ab' };
      const _p2 = { v: 'cd' };
      const _result = { v: 'ef' };
      const enqueuedTask = jest.fn(
        enqueueAsync((p1: any, p2: any) => {
          expect(p1).toBe(_p1);
          expect(p2).toBe(_p2);
          return Promise.resolve(_result);
        })
      );
      await expect(enqueuedTask(_p1, _p2)).resolves.toBe(_result);
    });

    it('should execute immediatly the first task', async () => {
      const localEnqueueAsync = enqueueAsync();

      const enqueuedTask1 = localEnqueueAsync(task1);
      const timer = createTimer();
      const promise = enqueuedTask1(task1);
      expect(task1.mock.calls.length).toBe(1);

      await expect(promise).resolves.toBeUndefined();
      expect(timer.elapsedTimeIsAbout(task1DurationMs)).toBe(true);
    });

    it('should enqueue 3 tasks without delay', async () => {
      const localEnqueueAsync = enqueueAsync();

      const enqueuedTask1 = localEnqueueAsync(task1);
      const enqueuedTask2 = localEnqueueAsync(task2);
      expect(task1.mock.calls.length).toBe(0);
      expect(task2.mock.calls.length).toBe(0);

      const timer = createTimer();
      const t1 = enqueuedTask1();
      const t2 = enqueuedTask2();
      const t3 = enqueuedTask1();
      expect(task1.mock.calls.length).toBe(1);
      expect(task2.mock.calls.length).toBe(0);

      await expect(t1).resolves.toBeUndefined();
      expect(task1.mock.calls.length).toBe(1);
      expect(task2.mock.calls.length).toBe(1); // the next task is already started here
      expect(timer.elapsedTimeIsAbout(task1DurationMs)).toBe(true);

      await expect(t2).resolves.toBeUndefined();
      expect(task1.mock.calls.length).toBe(2); // the next task is already started here
      expect(task2.mock.calls.length).toBe(1);
      expect(timer.elapsedTimeIsAbout(task1DurationMs + task2DurationMs)).toBe(true);

      await expect(t3).resolves.toBeUndefined();
      expect(task1.mock.calls.length).toBe(2);
      expect(task2.mock.calls.length).toBe(1);
      expect(timer.elapsedTimeIsAbout(task1DurationMs * 2 + task2DurationMs)).toBe(true);
    });

    it('should enqueue 3 tasks with a delay between each', async () => {
      const delayMs = 50;
      const localEnqueueAsync = enqueueAsync({ delayMs });

      const enqueuedTask1 = localEnqueueAsync(task1);
      const enqueuedTask2 = localEnqueueAsync(task2);
      expect(task1.mock.calls.length).toBe(0);
      expect(task2.mock.calls.length).toBe(0);

      const timer = createTimer();
      const t1 = enqueuedTask1();
      const t2 = enqueuedTask2();
      const t3 = enqueuedTask1();
      expect(task1.mock.calls.length).toBe(1);
      expect(task2.mock.calls.length).toBe(0);

      await expect(t1).resolves.toBeUndefined();
      expect(task1.mock.calls.length).toBe(1);
      expect(task2.mock.calls.length).toBe(0);
      expect(timer.elapsedTimeIsAbout(task1DurationMs)).toBe(true);

      await expect(t2).resolves.toBeUndefined();
      expect(task1.mock.calls.length).toBe(1);
      expect(task2.mock.calls.length).toBe(1);
      expect(timer.elapsedTimeIsAbout(Math.max(task1DurationMs, delayMs) + task2DurationMs)).toBe(true);

      await expect(t3).resolves.toBeUndefined();
      expect(task1.mock.calls.length).toBe(2);
      expect(task2.mock.calls.length).toBe(1);
      expect(
        timer.elapsedTimeIsAbout(
          Math.max(task1DurationMs, delayMs) + Math.max(task2DurationMs, delayMs) + task1DurationMs
        )
      ).toBe(true);
    });
  });

  describe('enqueueOrMergeAsync', () => {
    const taskDurationMs = 20;
    let task: jest.Mock<Promise<string>>;

    beforeEach(() => {
      task = jest.fn((val: string) => new Promise<string>((resolve) => setTimeout(() => resolve(val), taskDurationMs)));
    });

    it('pass parameters to the task', async () => {
      const _p1 = { v: 'ab' };
      const _p2 = { v: 'cd' };
      const _result = { v: 'ef' };
      const enqueuedTask = jest.fn(
        enqueueOrMergeAsync((p1: any, p2: any) => {
          expect(p1).toBe(_p1);
          expect(p2).toBe(_p2);
          return Promise.resolve(_result);
        })
      );
      await expect(enqueuedTask(_p1, _p2)).resolves.toBe(_result);
    });

    it('should execute immediatly the first task', async () => {
      const enqueuedTask = enqueueOrMergeAsync(task);
      const timer = createTimer();
      const promise = enqueuedTask();
      expect(task.mock.calls.length).toBe(1);

      await expect(promise).resolves.toBeUndefined();
      expect(timer.elapsedTimeIsAbout(taskDurationMs)).toBe(true);
    });

    it('should merge 3 tasks without minimal delay between', async () => {
      const enqueuedTask = enqueueOrMergeAsync(task);
      expect(task.mock.calls.length).toBe(0);

      await executeMergedEnqueuedTasks({ enqueuedTask, task, taskDurationMs });
    });

    it('should merge 3 tasks twice', async () => {
      const delayMs = 80;
      const enqueuedTask = enqueueOrMergeAsync(task, { delayMs });
      expect(task.mock.calls.length).toBe(0);
      const timer = createTimer();

      await executeMergedEnqueuedTasks({ enqueuedTask, task, taskDurationMs });
      expect(timer.elapsedTimeIsAbout(taskDurationMs)).toBe(true);

      const remainingDelayMs = delayMs - taskDurationMs;
      await executeMergedEnqueuedTasks({ enqueuedTask, task, taskDurationMs, delayedDelayMs: remainingDelayMs });
      expect(timer.elapsedTimeIsAbout(Math.max(taskDurationMs, delayMs) + taskDurationMs)).toBe(true);
    });
  });
});

async function executeMergedEnqueuedTasks({
  enqueuedTask,
  task,
  taskDurationMs,
  delayedDelayMs = 0,
}: {
  enqueuedTask: jest.Mock<Promise<string>, any>;
  task: jest.Mock<Promise<string>>;
  taskDurationMs: number;
  delayedDelayMs?: number;
}) {
  const initialCallsLength = task.mock.calls.length;
  const firstVal = `a${Math.round(Math.random() * 100)}`;
  const timer = createTimer({ allowShorterElapsedTime: true });
  const t1 = enqueuedTask(firstVal);
  const t2 = enqueuedTask('b');
  const t3 = enqueuedTask('c');

  const withImmediateCall = delayedDelayMs === 0;
  expect(task.mock.calls.length).toBe(initialCallsLength + (withImmediateCall ? 1 : 0));
  const estimatedTime = delayedDelayMs + taskDurationMs;

  await expect(t1).resolves.toBe(firstVal);
  expect(task.mock.calls.length).toBe(initialCallsLength + 1);
  expect(timer.elapsedTimeIsAbout(estimatedTime)).toBe(true);

  await expect(t2).resolves.toBe(firstVal);
  expect(task.mock.calls.length).toBe(initialCallsLength + 1);
  expect(timer.elapsedTimeIsAbout(estimatedTime)).toBe(true);

  await expect(t3).resolves.toBe(firstVal);
  expect(task.mock.calls.length).toBe(initialCallsLength + 1);
  expect(timer.elapsedTimeIsAbout(estimatedTime)).toBe(true);
}

function createTimer({ allowShorterElapsedTime = false } = {}) {
  const thresholdMs = 20;
  const initialTs = Date.now();
  let startTs = initialTs;
  return {
    get initialTs() {
      return initialTs;
    },
    get startTs() {
      return startTs;
    },
    reset() {
      startTs = Date.now();
    },
    elapsedTimeIsAbout(ms: number) {
      const diff = Date.now() - startTs;
      const result = (allowShorterElapsedTime ? diff >= ms - thresholdMs : diff >= ms) && diff <= ms + thresholdMs;
      return result ? true : `expected ${ms}ms, got ${diff}ms`;
    },
    elapsedTimeAsString() {
      const diff = Date.now() - startTs;
      return `${diff}ms`;
    },
    elapsedFromInitialTimeAsString() {
      const diff = Date.now() - initialTs;
      return `${diff}ms`;
    },
  };
}
