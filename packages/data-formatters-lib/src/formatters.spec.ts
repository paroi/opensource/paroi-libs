import {
	boolVal,
	boolValOrUndef,
	dateVal,
	dateValOrUndef,
	listVal,
	listValOrUndef,
	nbVal,
	nbValOrUndef,
	strVal,
	strValOrUndef,
} from "./formatters";

describe("Helpers - formaters", () => {
	describe("boolVal", () => {
		testBoolVal(boolVal);

		it("should throw error if value is undefined, null or empty", () => {
			expect(() => boolVal(undefined)).toThrow();
			expect(() => boolVal(null)).toThrow();
		});
	});

	describe("boolValOrUndef", () => {
		testBoolVal(boolValOrUndef);

		it("should throw error if value is undefined or null", () => {
			expect(boolValOrUndef(undefined)).toBeUndefined();
			expect(boolValOrUndef(null)).toBeUndefined();
		});
	});

	describe("strVal", () => {
		testStrVal(strVal);

		it("should throw error if value is undefined, null or empty", () => {
			expect(() => strVal(undefined)).toThrow();
			expect(() => strVal(null)).toThrow();
			expect(() => strVal("")).toThrow();
		});

		it("should allow empty string if option 'allowEmpty' is enabled", () => {
			expect(strVal("", { allowEmpty: true })).toBe("");
		});
	});

	describe("strValOrUndef", () => {
		testStrVal(strValOrUndef);

		it("should return undefined if value is undefined, null or empty", () => {
			expect(strValOrUndef(undefined)).toBeUndefined();
			expect(strValOrUndef(null)).toBeUndefined();
			expect(strValOrUndef("")).toBeUndefined();
		});

		it("should return empty string if option 'allowEmpty' is enabled", () => {
			expect(strValOrUndef("", { allowEmpty: true })).toBe("");
		});
	});

	describe("nbVal", () => {
		testNbVal(nbVal);

		it("should throw error if value is undefined or null", () => {
			expect(() => nbVal(undefined)).toThrow();
			expect(() => nbVal(null)).toThrow();
		});
	});

	describe("nbValOrUndef", () => {
		testNbVal(nbValOrUndef);

		it("should return undefined if value is undefined or null", () => {
			expect(nbValOrUndef(undefined)).toBeUndefined();
			expect(nbValOrUndef(null)).toBeUndefined();
		});
	});

	describe("dateVal", () => {
		testDateVal(dateVal);

		it("should throw error if value is undefined, null or empty", () => {
			expect(() => dateVal(undefined)).toThrow();
			expect(() => dateVal(null)).toThrow();
			expect(() => dateVal("")).toThrow();
		});
	});

	describe("dateValOrUndef", () => {
		testDateVal(dateValOrUndef);

		it("should return undefined if value is undefined, null or empty", () => {
			expect(dateValOrUndef(undefined)).toBeUndefined();
			expect(dateValOrUndef(null)).toBeUndefined();
			expect(dateValOrUndef("")).toBeUndefined();
		});
	});

	describe("listVal", () => {
		testListVal(listVal);

		it("should throw an error if the value is not defined", () => {
			const formater = jest.fn((item) => item);
			expect(() => listVal(undefined, formater)).toThrow();
			expect(formater).not.toHaveBeenCalled();
			expect(() => listVal(null, formater)).toThrow();
			expect(formater).not.toHaveBeenCalled();
		});
	});

	describe("listValOrUndef", () => {
		testListVal(listValOrUndef);

		it("should return undefined if value is not defined", () => {
			const formater = jest.fn((item) => item);
			expect(listValOrUndef(undefined, formater)).toBeUndefined();
			expect(formater).not.toHaveBeenCalled();
			expect(listValOrUndef(null, formater)).toBeUndefined();
			expect(formater).not.toHaveBeenCalled();
		});
	});
});

function testBoolVal(helper: (val: unknown) => any) {
	it('should return true if value is the string "true"', () => {
		expect(helper("true")).toBe(true);
		expect(helper("t")).toBe(true);
	});

	it('should return false if value is the string "false"', () => {
		expect(helper("false")).toBe(false);
		expect(helper("f")).toBe(false);
	});

	it('should return true if value is 1 or "1"', () => {
		expect(helper(1)).toBe(true);
		expect(helper("1")).toBe(true);
	});

	it('should return false if value is 0 or "0"', () => {
		expect(helper(0)).toBe(false);
		expect(helper("0")).toBe(false);
	});

	it("should convert any truthy / falsy values", () => {
		expect(helper(12)).toBe(true);
		expect(helper(Symbol())).toBe(true);
		expect(helper([])).toBe(true);
		expect(helper({})).toBe(true);
		expect(helper("")).toBe(false);
	});
}

function testStrVal(helper: (val: unknown) => any) {
	it("should return the same value if value is a string", () => {
		const value = "foo";
		expect(helper(value)).toBe(value);
	});
}

function testNbVal(helper: (val: unknown) => any) {
	it("should return the same value if value is a number", () => {
		const value = 1;
		expect(helper(value)).toBe(value);
	});

	it("should convert string value to a number", () => {
		const value = "1";
		expect(helper(value)).toBe(Number(value));
	});

	it("should throw error if the type of value is not supported", () => {
		expect(() => helper(new Date())).toThrow();
	});
}

function testDateVal(helper: (val: unknown) => any) {
	it("should return the same value if value is a Date", () => {
		const value = new Date("2022-05-19T14:04:33.204Z");
		expect(helper(value)).toBe(value);
	});

	it("should convert string value to a Date", () => {
		const value = "2022-05-19T14:04:33.204Z";
		const result = helper(value);
		expect(result).toBeInstanceOf(Date);
		expect(result.getTime()).toBe(new Date(value).getTime());
	});

	it("should convert timestamp (number) value to a Date", () => {
		const value = new Date("2022-05-19T14:04:33.204Z").getTime();
		const result = helper(value);
		expect(result).toBeInstanceOf(Date);
		expect(result.getTime()).toBe(new Date(value).getTime());
	});

	it("should throw error if the type of value is not supported", () => {
		const value = true;
		expect(() => helper(value)).toThrow();
	});
}

function testListVal(helper: (val: unknown, valueFormater: (val: unknown) => any) => any) {
	it("should call the formater for each list item", () => {
		const formater = jest.fn((item) => item);
		const list = ["a", "b"];
		const result = helper(list, formater);
		expect(formater.mock.calls.length).toBe(list.length);
		expect(result).toStrictEqual(list);
	});

	it("should throw an error if the value is not an array", () => {
		expect(() => helper(true, () => {})).toThrow();
	});
}
