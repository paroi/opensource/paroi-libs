export function errorMessageSuffix(options: { varName?: string }) {
	return options.varName ? ` for '${options.varName}'` : "";
}
