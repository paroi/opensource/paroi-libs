import { errorMessageSuffix } from "./_helpers";
import { isNumeric } from "./public-helpers";

export function boolVal(val: unknown, options: { varName?: string } = {}): boolean {
	if (typeof val === "boolean") return val;
	if (val === 0 || val === "0" || val === "f" || val === "false") return false;
	if (val === 1 || val === "1" || val === "t" || val === "true") return true;
	if (val === undefined || val === null) {
		throw new Error(`Missing boolean value${errorMessageSuffix(options)}`);
	}
	return !!val;
}

export function boolValOrUndef(
	val: unknown,
	options: { varName?: string } = {},
): boolean | undefined {
	return val === undefined || val === null ? undefined : boolVal(val, options);
}

export function strVal(
	val: unknown,
	options: { allowEmpty?: boolean; varName?: string } = {},
): string {
	if (val === undefined || val === null || (val === "" && !options.allowEmpty)) {
		throw new Error(`Missing string value${errorMessageSuffix(options)}`);
	}
	if (typeof val === "string") return val;
	return (val as object).toString();
}

export function strValOrUndef(
	val: unknown,
	options: { allowEmpty?: boolean; varName?: string } = {},
): string | undefined {
	return val === undefined || val === null || (val === "" && !options.allowEmpty)
		? undefined
		: strVal(val, options);
}

export function nbVal(val: unknown, options: { varName?: string } = {}): number {
	if (typeof val === "number") return val;
	if (val === undefined || val === null) {
		throw new Error(`Missing number value${errorMessageSuffix(options)}`);
	}
	if (typeof val === "string" && isNumeric(val)) return Number(val);
	throw new Error(
		`Cannot convert to number the value of type '${typeof val}'${errorMessageSuffix(options)}`,
	);
}

export function nbValOrUndef(val: unknown, options: { varName?: string } = {}): number | undefined {
	return val === undefined || val === null ? undefined : nbVal(val, options);
}

export function dateVal(val: unknown, options: { varName?: string } = {}): Date {
	if (val instanceof Date) return val;
	if (val === undefined || val === null || val === "") {
		throw new Error(`Missing number value${errorMessageSuffix(options)}`);
	}
  if (typeof val === "string") {
    const hasTz = val.includes("Z") || val.includes("+");
    return new Date(hasTz ? val : `${val}Z`);
  }
	if (typeof val === "number") return new Date(val);
	throw new Error(
		`Cannot convert to date the value of type '${typeof val}'${errorMessageSuffix(options)}`,
	);
}

export function dateValOrUndef(val: unknown, options: { varName?: string } = {}): Date | undefined {
	return val === undefined || val === null || val === "" ? undefined : dateVal(val, options);
}

export function listVal<T>(
	val: unknown,
	valueFormatter: (val: unknown) => T,
	options: { varName?: string } = {},
): T[] {
	if (!Array.isArray(val)) {
		throw new Error(`Invalid array '${typeof val}'${errorMessageSuffix(options)}`);
	}
	return val.map(valueFormatter);
}

export function listValOrUndef<T>(
	val: unknown,
	valueFormatter: (val: unknown) => T,
	options: { varName?: string } = {},
): T[] | undefined {
	return val === undefined || val === null ? undefined : listVal(val, valueFormatter, options);
}
