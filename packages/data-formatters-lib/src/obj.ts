import { errorMessageSuffix } from "./_helpers";

export interface Obj {
  [key: string]: unknown;
}

export function isObj(val: unknown): val is Obj {
  return !!val && typeof val === "object" && !Array.isArray(val);
}

export function objVal(val: unknown, options: { varName?: string } = {}): Obj {
  if (isObj(val)) return val;
  const valStr = val === null ? "null" : Array.isArray(val) ? "array" : typeof val;
  throw new Error(`invalid object value '${valStr}'${errorMessageSuffix(options)}`);
}

export function isDef<T>(val: T): val is NonNullable<T> {
  return val !== undefined && val !== null;
}
