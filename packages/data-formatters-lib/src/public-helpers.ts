export function messageOf(error: unknown): string {
  if (error === null) return "null";
  if (error === undefined) return "undefined";
  if (error instanceof Error) return error.message;
  switch (typeof error) {
    case "bigint":
    case "number":
    case "boolean":
      return String(error);
    case "string":
      return error;
    case "symbol":
      return `Symbol(${error.description ?? ""})`;
    case "object":
      return String("message" in error ? error.message : error);
    case "function":
      return `function ${error.name}`;
    default:
      return "invalid error type";
  }
}

/**
 * @see https://stackoverflow.com/a/175787/3786294
 */
export function isNumeric(str: string): boolean {
	return (
		// use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
		!isNaN(str as any) && !isNaN(parseFloat(str))
	); // ...and ensure strings of whitespace fail
}
